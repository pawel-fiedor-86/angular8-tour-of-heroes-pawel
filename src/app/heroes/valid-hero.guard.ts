import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {HeroesService} from './heroes.service';
import {map} from 'rxjs/operators';
import {Hero} from './hero';

@Injectable({
  providedIn: 'root'
})
export class ValidHeroGuard implements CanActivate {
  constructor(
    private heroesService: HeroesService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.heroesService.getHero(+next.paramMap.get('id')).pipe(
      map<Hero, boolean>(hero => !!hero)
    );
  }
}
