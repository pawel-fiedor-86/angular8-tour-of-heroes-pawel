import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeroesRoutingModule } from './heroes-routing.module';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroDetailsComponent } from './hero-details/hero-details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroesService } from './heroes.service';
import { HeroRegisterComponent } from './hero-register/hero-register.component';
import {
  MatGridListModule, MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule
} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { MessagesModule } from '../messages/messages.module';


@NgModule({
  declarations: [HeroListComponent, HeroDetailsComponent, DashboardComponent, HeroRegisterComponent],
  imports: [
    CommonModule,
    HeroesRoutingModule,
    MessagesModule,
    FormsModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [HeroesService]
})
export class HeroesModule { }
