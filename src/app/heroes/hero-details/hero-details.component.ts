import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../heroes.service';
import { Hero } from '../hero';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.scss']
})
export class HeroDetailsComponent implements OnInit {
  // Currently viewed hero
  hero: Hero;

  constructor(
    private heroesService: HeroesService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit() {
    this.fetchHero(+this.route.snapshot.paramMap.get('id'));
  }

  /**
   * Fetch hero by id
   * @param id Hero id
   */
  fetchHero(id: number): void {
    this.heroesService.getHero(id).subscribe(hero => this.hero = hero);
  }

  /**
   * Update hero
   * @param hero Hero data 
   */
  onSave(hero: Hero): void {
    this.heroesService.updateHero(hero).subscribe(_ => this.location.back());
  }
}
