import { Component, OnInit } from '@angular/core';
import { Heroes } from '../mock-heroes';
import { HeroesService } from '../heroes.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // Top heroes
  topHeroes: Heroes;

  constructor(private heroesService: HeroesService) { }

  ngOnInit() {
    this.fetchTopHeroes();
  }

  // Fetch top heroes
  fetchTopHeroes(): void {
    this.heroesService.getHeroes().subscribe(heroes => this.topHeroes = heroes.slice(1, 5));
  }
}
