import { TestBed, async, inject } from '@angular/core/testing';

import { ValidHeroGuard } from './valid-hero.guard';

describe('ValidHeroGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidHeroGuard]
    });
  });

  it('should ...', inject([ValidHeroGuard], (guard: ValidHeroGuard) => {
    expect(guard).toBeTruthy();
  }));
});
