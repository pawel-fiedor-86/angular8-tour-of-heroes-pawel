import { TestBed } from '@angular/core/testing';

import { MessagesService } from './messages.service';
import { MessagesModule } from './messages.module';

describe('MessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [MessagesModule]
  }));

  it('should be created', () => {
    const service: MessagesService = TestBed.get(MessagesService);
    expect(service).toBeTruthy();
  });
});
