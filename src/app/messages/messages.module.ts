import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesService } from './messages.service';
import { MessagesComponent } from './messages.component';
import { MatListModule, MatButtonModule } from '@angular/material';



@NgModule({
  declarations: [ MessagesComponent ],
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule
  ],
  providers: [
    MessagesService, // Ensure that MessagesService is provided by the module
  ],
  exports: [ MessagesComponent ]
})
export class MessagesModule { }
